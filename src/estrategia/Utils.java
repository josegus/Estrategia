package estrategia;

import java.util.Random;

public final class Utils {
    public static int getRandomNumber(int min, int max) {
        Random ran = new Random();       
        int x = ran.nextInt(max - min + 1) + min;
        return x;
    }    
}
