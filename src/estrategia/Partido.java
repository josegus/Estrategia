package estrategia;

import java.util.Random;

public class Partido {
    
    public int golesEquipo;
    public int golesRival;
    public int tiempo;
    private IStrategy estrategia;
    
    public Partido() {
        
    }
    
    // Se define la estrategia a tomar, en base al resultado
    // y tiempo transcurrido del partido
    public void setEstrategia(IStrategy estrategia) {
        this.estrategia = estrategia;
    }    
    
    public void mostrarEstrategia() {
        estrategia.mentalidad();
    }
}