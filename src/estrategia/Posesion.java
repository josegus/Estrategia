package estrategia;

public class Posesion implements IStrategy {
    
    public Posesion() {
    
    }
    
    @Override
    public void mentalidad() {
        System.out.println("Estrategia: Mantener la posesión y acercarse al arco rival");
    }
}
