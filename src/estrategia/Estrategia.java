/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estrategia;

import java.util.Random;

/**
 *
 * @author Gustavo
 */
public class Estrategia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Crear instancia de partido y definir (aleatoriamente)
        // el resultado parcial y tiempo del partido
        Partido partido = new Partido();
        partido.golesEquipo = Utils.getRandomNumber(0, 2);
        partido.golesRival = Utils.getRandomNumber(0, 2);
        partido.tiempo = Utils.getRandomNumber(70, 90);
        
        // Dependiendo del partido, definir estrategia a tomar
        // Si el tiempo es mayor a 85 el partido se está terminando, 
        // definir estrategia ya!
        if (partido.tiempo > 85) {
            if (partido.golesEquipo <= partido.golesRival) {
                partido.setEstrategia(new Ataque());
            } else {
                partido.setEstrategia(new Defensa());
            }
        } else { // tiempo <= 85, aún hay tiempo para pensar con calma
            partido.setEstrategia(new Posesion());
        }
        
        // Ver estadisticas derl partido y mentalidad del equipo
        System.out.println("Tiempo: " + partido.tiempo);
        System.out.println("Goles equipo: " + partido.golesEquipo);
        System.out.println("Goles rival: " + partido.golesRival);
        partido.mostrarEstrategia();
        System.out.println();
    }
    
    public int getRandomNumberr(int min, int max) {
        Random ran = new Random();       
        int x = ran.nextInt(max - min + 1) + min;
        return x;
    }
}
