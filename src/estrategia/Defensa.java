package estrategia;

public class Defensa implements IStrategy {
    
    public Defensa() {
    
    }
    
    @Override
    public void mentalidad() {
        System.out.println("Estrategia: Todos a colgarse del arco");
    }
}
